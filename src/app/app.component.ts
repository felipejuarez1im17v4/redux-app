import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 calculo!:number

 constructor(){
  this.calculo = 10
 }

incremental(){
  this.calculo =this.calculo + 1;
}

decremental(){
 this.calculo = this.calculo - 1;
}

}
